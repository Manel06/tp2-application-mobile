package com.example.tp2v2;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class WineActivity extends AppCompatActivity {

    final WineDbHelper dbHelper = new WineDbHelper(WineActivity.this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wine);

        final EditText titre = (EditText) findViewById(R.id.wineName);
        final EditText region = (EditText) findViewById(R.id.editWineRegion);
        final EditText loc = (EditText) findViewById(R.id.editLoc);
        final EditText climat = (EditText) findViewById(R.id.editClimate);
        final EditText surface = (EditText) findViewById(R.id.editPlantedArea);

        final Button button = (Button) findViewById(R.id.button);

        Intent intent = getIntent();
        if (intent != null) {
            if (intent.hasExtra("wine")) {

                final Wine wine = intent.getParcelableExtra("wine");
                if (wine != null) {

                    titre.setText(wine.getTitle());
                    region.setText(wine.getRegion());
                    loc.setText(wine.getLocalization());
                    climat.setText(wine.getClimate());
                    surface.setText(wine.getPlantedArea());
                }

                button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (wine != null) {

                            wine.setTitle(titre.getText().toString());
                            wine.setRegion(region.getText().toString());
                            wine.setLocalization(loc.getText().toString());
                            wine.setClimate(climat.getText().toString());
                            wine.setPlantedArea(surface.getText().toString());

                            dbHelper.updateWine(wine);

                            Toast.makeText(WineActivity.this, "test1", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(WineActivity.this, MainActivity.class);
                            startActivity(intent);
                        }
                        else{
                            Wine newWine = new Wine();

                            newWine.setTitle(titre.getText().toString());
                            newWine.setRegion(region.getText().toString());
                            newWine.setLocalization(loc.getText().toString());
                            newWine.setClimate(climat.getText().toString());
                            newWine.setPlantedArea(surface.getText().toString());

                            if (!titre.getText().toString().equals("")){

                                dbHelper.addWine(newWine);
                                Intent intent = new Intent(WineActivity.this, MainActivity.class);
                                startActivity(intent);
                            }
                            else{
                                AlertDialog alertDialog = new AlertDialog.Builder(WineActivity.this).create();
                                alertDialog.setTitle("Sauvegarde impossible");
                                alertDialog.setMessage("Le nom du vin doit être non vide.");
                                alertDialog.show();
                            }

                        }
                    }
                });
            }
        }
    }
}
