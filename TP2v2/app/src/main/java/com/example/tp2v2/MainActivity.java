package com.example.tp2v2;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    WineDbHelper dbHelper;
    SQLiteDatabase db;
    SimpleCursorAdapter cursorAdapter;
    ListView listview;
    Cursor cursor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        dbHelper = new WineDbHelper(this);
        db = dbHelper.getReadableDatabase();

        //dbHelper.onUpgrade(db, 1, 1);
        //dbHelper.populate();

        if (dbHelper.fetchAllWines().getCount() < 1) {
            dbHelper.populate();
        }

        //final Cursor cursor = db.query(WineDbHelper.TABLE_NAME, new String[]{WineDbHelper._ID, WineDbHelper.COLUMN_NAME, WineDbHelper.COLUMN_WINE_REGION, WineDbHelper.COLUMN_LOC, WineDbHelper.COLUMN_CLIMATE, WineDbHelper.COLUMN_PLANTED_AREA}, null, null, null, null, WineDbHelper.COLUMN_NAME);
        //cursor.moveToFirst();

        listview = (ListView) findViewById(R.id.listview);
        cursor = dbHelper.fetchAllWines();

        cursorAdapter = new SimpleCursorAdapter(this, android.R.layout.simple_list_item_2, cursor, new String[]{dbHelper.COLUMN_NAME, dbHelper.COLUMN_WINE_REGION}, new int[]{android.R.id.text1, android.R.id.text2}, 0);
        listview.setAdapter(cursorAdapter);
        registerForContextMenu(listview);

        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Cursor itemWine = (Cursor) parent.getItemAtPosition(position);

                Intent intent = new Intent(MainActivity.this, WineActivity.class);
                intent.putExtra("wine", dbHelper.cursorToWine(itemWine));
                startActivity(intent);
            }
        });

        final FloatingActionButton ajout = (FloatingActionButton) findViewById(R.id.fab);
        ajout.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, WineActivity.class);
                intent.putExtra("wine", false);
                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {

        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.context_menu, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {

        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        cursor.moveToPosition(info.position);

        if(item.getItemId() == R.id.delete) {

            dbHelper.deleteWine(cursor);

            cursor = dbHelper.fetchAllWines();
            cursorAdapter.changeCursor(cursor);
            cursorAdapter.notifyDataSetChanged();

            Toast.makeText(MainActivity.this, "Suppression", Toast.LENGTH_SHORT).show();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
